# Element Zero

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)  
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

## Usage

I would prefer that you didn't attempt to run this module on your own bots.
Instead, please [add my bot to your server](https://discordapp.com/oauth2/authorize?client_id=357901631460737026&scope=bot),
which has the added bonus of saving you from a lot of hassle that set-up of a bot on this scale might cause.

The source code is provided here in the hopes that some people can learn from them, and learn from my mistakes.

## Support

Support for the official bot can be found by issuing a `e0support` command.
Support for running this module on your own bots is not provided.
