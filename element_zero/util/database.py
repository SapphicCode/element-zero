import asyncio
import datetime
import random

import pymongo
from discord.ext import commands
from discord.utils import snowflake_time

from element_zero.util import errors


# noinspection PyShadowingBuiltins
class DatabaseInterface:
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        mongo = bot.get_cog('DB').mongo
        self.db = mongo.get_database('element_zero')

        self.col_backend_guilds = self.db.get_collection('guilds')
        self.col_emojis = self.db.get_collection('emojis')
        self.col_emoji_usage = self.db.get_collection('emoji_usage')
        self.col_settings = self.db.get_collection('settings')

        indexes = [
            # guilds
            self.col_backend_guilds.create_index([('id', pymongo.ASCENDING)], unique=True),
            # emojis
            self.col_emojis.create_index([('id', pymongo.ASCENDING)], unique=True),
            self.col_emojis.create_index([('owner', pymongo.ASCENDING)]),
            self.col_emojis.create_index([('animated', pymongo.ASCENDING)]),
            self.col_emojis.create_index([('name_lwr', pymongo.ASCENDING)], unique=True),
            # settings
            self.col_settings.create_index([('id', pymongo.ASCENDING)], unique=True),
            # usage logs
            self.col_emoji_usage.create_index([('at', pymongo.ASCENDING)]),
        ]
        self.bot.loop.create_task(asyncio.gather(*indexes))

        self.static_emoji_cap = 50
        self.animated_emoji_cap = 50

        self.user_emoji_cap = 50

        self.valid_name_chars = 'abcdefghijklmnopqrstuvwxyz0123456789-_'

    @staticmethod
    def to_discord_emoji(data):
        _id = data['id']
        name = data['name']
        animated = data.get('animated', False)
        if animated:
            return f'<a:{name}:{_id}>'
        return f'<:{name}:{_id}>'

    def check_name_validity(self, name):
        name = name.lower()
        for char in name:
            if char not in self.valid_name_chars:
                raise errors.InvalidCharacterInName(char)

    async def list_backend_guilds(self, shuffle=True):
        guilds = [
            self.bot.get_guild(x['id'])
            async for x in self.col_backend_guilds.find() if self.bot.get_guild(x['id']) is not None
        ]

        if shuffle:
            random.shuffle(guilds)
        return guilds

    async def find_free_guild(self, animated=False):
        cap = self.static_emoji_cap if not animated else self.animated_emoji_cap
        guilds = await self.list_backend_guilds(shuffle=True)
        for guild in guilds:
            if guild.unavailable:
                continue
            if len([x for x in guild.emojis if x.animated == animated]) >= cap:
                continue
            return guild

    async def enumerate_emojis(self, animated=None):
        if animated is not None:
            return await self.col_emojis.count_documents({'animated': animated})
        return await self.col_emojis.count_documents({})

    async def _settings_for(self, entity_id):
        s = await self.col_settings.find_one({'id': entity_id})
        if s is None:
            s = {}
        return s

    async def get_user_slots_used(self, uid):
        return await self.col_emojis.count_documents({'owner': uid, 'verification': {'$exists': 'false'}})

    async def get_user_slots_capacity(self, uid):
        s = await self._settings_for(uid)
        return s.get('slots', self.user_emoji_cap)

    async def get_emojis_to_delete(self, before, min_usage):
        match = {'$match': {'at': {'$gt': before}}}
        emojis = await self.get_emojis()
        usage = await self.get_emoji_usage(match)

        out = []
        for emoji in emojis:
            if emoji.get('verification'):
                continue
            if snowflake_time(emoji['id']) > before:
                continue
            if usage.get(emoji['id'], 0) >= min_usage:
                continue
            out.append(emoji)

        return out

    async def get_user_blacklist(self, uid):
        s = await self._settings_for(uid)
        return s.get('blacklist')

    async def set_user_blacklist(self, uid, **settings):
        case = {}
        for setting, value in settings.items():
            case[f'blacklist.{setting}'] = value

        if case:
            case['updated_at'] = datetime.datetime.utcnow()
            return await self.col_settings.update_one({'id': uid}, {'$set': case}, upsert=True)
        else:
            case = await self.get_user_blacklist(uid)
            if not case:
                return
            case['removed_at'] = datetime.datetime.utcnow()
            actions = {'$unset': {'blacklist': None}, '$push': {'blacklist_history': case}}
            return await self.col_settings.update_one({'id': uid}, actions, upsert=True)

    async def get_emoji(self, name):
        return await self.col_emojis.find_one({'name_lwr': name.lower()})

    async def get_emojis_by_owner(self, owner_id):
        return [x async for x in self.col_emojis.find({'owner': owner_id})]

    async def get_emojis(self):
        return [x async for x in self.col_emojis.find({})]

    async def get_emoji_usage(self, *filters):
        return {
            x['_id']: x['count']
            async for x in
            self.col_emoji_usage.aggregate([*filters, {
                '$group': {
                    '_id': '$emoji',
                    'count': {
                        '$sum': 1
                    }
                }
            }])
        }

    async def create_emoji(self, name, owner_id, animated, bytes):
        # user checks:

        blacklist = await self.get_user_blacklist(owner_id)
        if blacklist:
            raise errors.UserBlacklisted(blacklist)

        used = await self.get_user_slots_used(owner_id)
        cap = await self.get_user_slots_capacity(owner_id)
        if used >= cap:
            raise errors.UserAtCapacity(cap)

        # everything else:

        self.check_name_validity(name)

        emoji = await self.get_emoji(name)
        if emoji:
            raise errors.EmojiExists()

        guild = await self.find_free_guild(animated)
        if guild is None:
            raise errors.BackendGuildsFull()

        # checks look good, proceeding

        emoji = await guild.create_custom_emoji(name=name, image=bytes)

        await self.col_emojis.insert_one(
            {
                'id': emoji.id,
                'name_lwr': name.lower(),
                'name': name,
                'owner': owner_id,
                'animated': emoji.animated
            }
        )
        self.bot.dispatch('stats_update')
        return await self.get_emoji(name)

    async def delete_emoji(self, name, verify_owner=None):
        name = name.lower()

        db_emoji = emoji = await self.get_emoji(name)
        if not emoji:
            raise errors.EmojiNotFound()
        if verify_owner and verify_owner != db_emoji['owner']:
            raise errors.UserVerificationFailed()

        emoji = self.bot.get_emoji(emoji['id'])
        if emoji is None:
            raise errors.DiscordInterfaceError()

        await emoji.delete()
        await self.col_emojis.delete_one({'id': db_emoji['id']})
        self.bot.dispatch('stats_update')
        return db_emoji

    async def find_user(self, uid):
        user = self.bot.get_user(uid)
        if user is None:
            return f'Unknown user (ID: {uid})'
        return f'{user.mention} ({user})'

    async def set_emoji_verification(self, name, moderator_id):
        await self.col_emojis.update_one({'name_lwr': name.lower()}, {'$set': {'verification.moderator': moderator_id}})

    async def set_emoji_flag(self, name, state, moderator_id):
        await self.col_emojis.update_one(
            {
                'name_lwr': name.lower()
            }, {'$set': {
                'flag.moderator': moderator_id,
                'flag.state': state
            }}
        )

    async def log_emoji_use(self, emoji, user_id, guild_id):
        return await self.col_emoji_usage.insert_one(
            {
                'emoji': emoji['id'],
                'at': datetime.datetime.utcnow(),
                'user': user_id,
                'guild': guild_id
            }
        )

    async def set_user_state(self, user_id, state):
        return await self.col_settings.update_one({'id': user_id}, {'$set': {'state': state}}, upsert=True)

    async def set_guild_state(self, guild_id, state):
        return await self.set_user_state(guild_id, state)

    async def get_user_state(self, user_id):
        s = await self._settings_for(user_id)
        return s.get('state')

    async def get_guild_state(self, guild_id):
        return await self.get_user_state(guild_id)

    async def get_state(self, user_id, guild_id):
        state = True

        gstate = await self.get_guild_state(guild_id)
        if gstate is not None:
            state = gstate

        ustate = await self.get_user_state(user_id)
        if ustate is not None:
            state = ustate

        return state
